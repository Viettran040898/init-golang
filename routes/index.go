package routes

import (
	authentication "todo_server/modules/auth"
	user "todo_server/modules/users"
	"todo_server/services"

	"github.com/labstack/echo/v4"
)

func ActivateIndex(g *echo.Group) {
	services.UseRoute(g.Group("/users"), user.UserSubRoutes)
	services.UseRoute(g.Group("/auth"), authentication.AuthSubRoutes)
}
