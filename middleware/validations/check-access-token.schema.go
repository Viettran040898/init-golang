package validations

type CheckAccessTokenValidationSchema struct {
	AccessToken string `json:"accessToken" form:"accessToken" validate:"required"`
}
