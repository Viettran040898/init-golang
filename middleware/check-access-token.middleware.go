package middleware

import (
	"todo_server/constants"

	"github.com/labstack/echo/v4"
)

func CheckAccessToken(isRequired bool) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		constants.LOGGER.Info(constants.MIDDLEWARE + "::CheckAccessToken::is called")
		return func(c echo.Context) error {
			constants.LOGGER.Info(constants.MIDDLEWARE + "::CheckAccessToken::success")
			return next(c)
		}
	}
}
