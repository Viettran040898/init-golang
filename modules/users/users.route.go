package user

import (
	"github.com/labstack/echo/v4"
)

func UserSubRoutes(group *echo.Group) {
	group.GET("/:id", GetUser)
}
