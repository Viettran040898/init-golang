package user

import (
	"net/http"
	"todo_server/constants"
	"todo_server/models"

	"github.com/labstack/echo/v4"
)

func GetUser(c echo.Context) error {
	constants.LOGGER.Info(USERS_CONTROLLER + `::getUser::is called`)
	// User ID from path `users/:id`
	//id := c.Param("id")
	//idToInt, err := strconv.Atoi(id)
	var response *models.Response

	// if err != nil {
	// 	response = &models.Response{
	// 		Status:  http.StatusInternalServerError,
	// 		Message: err.Error(),
	// 		Data:    ""}

	// 	constants.LOGGER.Error(USERS_CONTROLLER+`::getUser::Error`, err.Error())
	// 	return c.JSONPretty(http.StatusInternalServerError, response, " ")
	// }

	// user := models.User{
	// 	ID:           primitive.NewObjectID(),
	// 	PasswordHash: "das",
	// 	PasswordSalt: "sda",
	// 	Email:        "dasdasd",
	// 	RefreshToken: "dsad",
	// 	OtpCode:      "sdad"}

	// err = InsertOneUser(user)

	// if err != nil {
	// 	response = &models.Response{
	// 		Status:  http.StatusInternalServerError,
	// 		Message: GET_USER_ID_HAS_BEEN_NUMBER,
	// 		Data:    err}

	// 	constants.LOGGER.Error(USERS_CONTROLLER+`::getUser::Error`, err)
	// 	return c.JSONPretty(http.StatusInternalServerError, response, " ")
	// }

	// student := &models.Student{
	// 	Id:    idToInt,
	// 	Name:  "Tue Vo",
	// 	Class: "19HCB",
	// 	Point: 6.3}
	response = &models.Response{
		Status:  http.StatusOK,
		Message: GET_USER_SUCCESS,
		Data:    ""}

	constants.LOGGER.Info(USERS_CONTROLLER + `::getUser::success`)
	return c.JSONPretty(http.StatusOK, response, " ")
}
