package user

import (
	"context"
	"todo_server/constants"
	"todo_server/database"
	"todo_server/models"

	"go.mongodb.org/mongo-driver/bson"
)

func InsertOneUser(user models.User) error {
	constants.LOGGER.Info(USERS_QUERY + "::InsertOneUser::is called")

	bBytes, err := bson.Marshal(user)

	if err != nil {
		constants.LOGGER.Error(USERS_QUERY+"::InsertOneUser::error", err.Error())
		return err
	}

	_, err = database.UsersCollection.InsertOne(
		context.Background(),
		bBytes)

	if err != nil {
		constants.LOGGER.Error(USERS_QUERY+"::InsertOneUser::error", err.Error())
		return err
	}

	constants.LOGGER.Info(USERS_QUERY + "::InsertOneUser::success")
	return nil
}

func FindUserHasConditions(conditions interface{}) (user models.User, err error) {
	constants.LOGGER.Info(USERS_QUERY + "::FindUserHasConditions::is called")
	user = models.User{}
	result := database.UsersCollection.FindOne(
		context.Background(),
		conditions)

	err = result.Decode(&user)

	if err != nil {
		constants.LOGGER.Error(USERS_QUERY+"::FindUserHasConditions::error", err.Error())
		return user, err
	}

	constants.LOGGER.Info(USERS_QUERY + "::FindUserHasConditions::success")
	return user, nil
}

func UpdateUser(conditions interface{}, valuesToUpdate interface{}) error {
	constants.LOGGER.Info(USERS_QUERY + "::UpdateUser::is called")

	_, err := database.UsersCollection.UpdateOne(
		context.Background(),
		conditions,
		valuesToUpdate)

	if err != nil {
		constants.LOGGER.Error(USERS_QUERY+"::UpdateUser::error", err.Error())
		return err
	}

	constants.LOGGER.Info(USERS_QUERY + "::UpdateUser::success")
	return nil
}

// var setElements bson.D
// if len(pivot.Base) > 0 {
//   setElements = append(setElements, bson.E{"base", pivot.Base})
// }
// if len(pivot.Email) > 0 {
//     setElements = append(setElements, bson.E{"email", pivot.Email})
// }

// setMap := bson.D{
//     {"$set", setElements},
// }
