package authentication

import (
	"net/http"
	"strings"
	"time"
	"todo_server/constants"
	"todo_server/models"
	"todo_server/modules/auth/validations"
	user "todo_server/modules/users"
	"todo_server/services"
	"todo_server/utils"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

func Login(c echo.Context) error {
	constants.LOGGER.Info(AUTH_CONTROLLER + "::Login::is called")
	schema := new(validations.LoginValidationSchema)
	err := utils.ValidateData(c, schema)
	var response *models.Response

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Login::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	email := strings.ToLower(schema.Email)
	conditions := bson.M{"email": email}
	user, err := user.FindUserHasConditions(conditions)

	//user not found
	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: LOGIN_USERNAME_OR_PASSWORD_NOT_MATCH,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Login::user not found", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(schema.Password))

	//password not match
	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: LOGIN_USERNAME_OR_PASSWORD_NOT_MATCH,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Login::password not match", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	//unverified account
	if !user.IsConfirmedOtp {
		response = &models.Response{
			Status:  http.StatusUnauthorized,
			Message: LOGIN_UNVERIFIED_ACCOUNT,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER + "::Login::unverified account")
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	//account is blocked
	if user.IsBlocked {
		response = &models.Response{
			Status:  http.StatusUnauthorized,
			Message: LOGIN_ACCOUNT_IS_BLOCKED,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER + "::Login::account is blocked")
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	accessToken, err := GenAccessToken(user.ID)

	//gen accessToken error
	if err != nil {
		response = &models.Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Login::Error", err.Error())
		return c.JSONPretty(http.StatusInternalServerError, response, " ")
	}

	data := map[string]interface{}{
		"user": map[string]interface{}{
			"fullName":  user.Fullname,
			"email":     user.Email,
			"createdAt": user.CreatedAt,
			"updatedAt": user.UpdatedAt,
			"role":      user.Role,
		},
		"meta": map[string]interface{}{
			"accessToken": accessToken,
		},
	}
	response = &models.Response{
		Status:  http.StatusOK,
		Message: LOGIN_SUCCESSFULLY,
		Data:    data}

	constants.LOGGER.Info(AUTH_CONTROLLER + "::Login::success")
	return c.JSONPretty(http.StatusOK, response, " ")
}

func Register(c echo.Context) error {
	constants.LOGGER.Info(AUTH_CONTROLLER + "::Register::is called")
	schema := new(validations.RegisterValidationSchema)
	err := utils.ValidateData(c, schema)
	var response *models.Response

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    ""}

		constants.LOGGER.Info(AUTH_CONTROLLER+"::Register::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	email := strings.ToLower(schema.Email)
	conditions := bson.M{"email": email}
	_, err = user.FindUserHasConditions(conditions)

	if err == nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: REGISTER_THE_EMAIL_WAS_REGISTERED,
			Data:    ""}

		constants.LOGGER.Info(AUTH_CONTROLLER + "::Register::the mail was registered")
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	if schema.ConfirmedPassword != schema.Password {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: REGISTER_PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCH,
			Data:    ""}

		constants.LOGGER.Info(AUTH_CONTROLLER + "::Register::Password and confirm password not match")
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(schema.Password), 10)
	otpCode := services.RandStringRunes(8)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Register::Error", err.Error())
		return c.JSONPretty(http.StatusInternalServerError, response, " ")
	}

	newUser := models.User{
		ID:           primitive.NewObjectID(),
		PasswordHash: string(passwordHash),
		Fullname:     schema.Fullname,
		Email:        email,
		RefreshToken: services.RandStringRunes(12),
		OtpCode:      otpCode,
		CreatedAt:    time.Now().UTC(),
		UpdatedAt:    time.Now().UTC(),
	}

	err = user.InsertOneUser(newUser)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Register::Error", err.Error())
		return c.JSONPretty(http.StatusInternalServerError, response, " ")
	}

	content := constants.OTP_CONFIRMATION_EMAIL_CONTENT
	content = strings.Replace(content, "NAME", schema.Fullname, -1)
	content = strings.Replace(content, "OTP", otpCode, -1)
	err = utils.SendMail(schema.Email, constants.OTP_CONFIRMATION_EMAIL_SUBJECT, content)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Register::Error", err.Error())
		return c.JSONPretty(http.StatusInternalServerError, response, " ")
	}

	response = &models.Response{
		Status:  http.StatusCreated,
		Message: REGISTER_SUCCESSFULLY,
		Data:    ""}

	constants.LOGGER.Info(AUTH_CONTROLLER + "::Register::success")
	return c.JSONPretty(http.StatusCreated, response, " ")
}

func ConfirmOtp(c echo.Context) error {
	constants.LOGGER.Info(AUTH_CONTROLLER + "::ConfirmOtp::is called")
	schema := new(validations.ConfirmOtpValidationSchema)
	err := utils.ValidateData(c, schema)
	var response *models.Response

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::ConfirmOtp::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	email := strings.ToLower(schema.Email)
	conditionsFindUser := bson.M{"email": email}
	userInfo, err := user.FindUserHasConditions(conditionsFindUser)

	//user not found
	if err != nil {
		response = &models.Response{
			Status:  http.StatusNotFound,
			Message: CONFIRM_OTP_OTP_CODE_DOES_NOT_EXIST,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::Login::user not found", err.Error())
		return c.JSONPretty(http.StatusNotFound, response, " ")
	}

	//otp not match
	if schema.OtpCode != userInfo.OtpCode {
		response = &models.Response{
			Status:  http.StatusNotFound,
			Message: CONFIRM_OTP_OTP_CODE_DOES_NOT_EXIST,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER + "::Login::otp not match")
		return c.JSONPretty(http.StatusNotFound, response, " ")
	}

	if userInfo.IsConfirmedOtp {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: CONFIRM_OTP_VERIFIED_ACCOUNT,
			Data:    ""}

		constants.LOGGER.Error(AUTH_CONTROLLER + "::Login::verified account")
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	var setElements bson.D
	conditions := bson.M{"_id": userInfo.ID}
	setElements = append(setElements, bson.E{"isConfirmedOtp", true})
	setElements = append(setElements, bson.E{"updatedAt", time.Now().UTC()})
	valuesToUpdate := bson.D{{"$set", setElements}}
	err = user.UpdateUser(conditions, valuesToUpdate)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::ConfirmOtp::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	response = &models.Response{
		Status:  http.StatusOK,
		Message: CONFIRM_OTP_COMFIRM_SUCCESSFUl,
		Data:    "",
	}

	constants.LOGGER.Info(AUTH_CONTROLLER + "::ConfirmOtp::success")
	return c.JSONPretty(http.StatusOK, response, " ")
}

func RefreshToken(c echo.Context) error {
	constants.LOGGER.Info(AUTH_CONTROLLER + "::RefreshToken::is called")
	schema := new(validations.RefreshTokenValidationSchema)
	err := utils.ValidateData(c, schema)
	var response *models.Response

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::RefreshToken::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	userId, err := VerifyToken(schema.AccessToken, false)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusUnauthorized,
			Message: REFRESH_TOKEN_INVALID_TOKEN,
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::RefreshToken::Error", err.Error())
		return c.JSONPretty(http.StatusUnauthorized, response, " ")
	}

	conditionsFindUser := bson.M{"_id": userId, "refreshToken": schema.RefreshToken}
	userInfo, err := user.FindUserHasConditions(conditionsFindUser)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusUnauthorized,
			Message: REFRESH_TOKEN_INVALID_TOKEN,
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::RefreshToken::Error", err.Error())
		return c.JSONPretty(http.StatusUnauthorized, response, " ")
	}

	accessToken, err := GenAccessToken(userInfo.ID)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::RefreshToken::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	data := map[string]interface{}{
		"accessToken": accessToken,
	}
	response = &models.Response{
		Status:  http.StatusOK,
		Message: REFRESH_TOKEN_REFRESH_TOKEN_SUCCESSFULLY,
		Data:    data,
	}

	constants.LOGGER.Info(AUTH_CONTROLLER + "::RefreshToken::success")
	return c.JSONPretty(http.StatusOK, response, " ")
}

func ResendOtp(c echo.Context) error {
	constants.LOGGER.Info(AUTH_CONTROLLER + "::ResendOtp::is called")
	schema := new(validations.ResendOtpValidationSchema)
	err := utils.ValidateData(c, schema)
	var response *models.Response

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::ResendOtp::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	email := strings.ToLower(schema.Email)
	conditionsFindUser := bson.M{"email": email}
	userInfo, err := user.FindUserHasConditions(conditionsFindUser)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusNotFound,
			Message: RESEND_OTP_CODE_EMAIL_NOT_FOUND,
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_SERVICE+"::ResendOtp::user not found", err.Error())
		return c.JSONPretty(http.StatusNotFound, response, " ")
	}

	if userInfo.IsConfirmedOtp {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: RESEND_OTP_CODE_VERIFIED_ACCOUNT,
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_SERVICE + "::ResendOtp::user not found")
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	otpCode := services.RandStringRunes(8)
	var setElements bson.D
	conditions := bson.M{"_id": userInfo.ID}
	setElements = append(setElements, bson.E{"otpCode", otpCode})
	setElements = append(setElements, bson.E{"updatedAt", time.Now().UTC()})
	valuesToUpdate := bson.D{{"$set", setElements}}
	err = user.UpdateUser(conditions, valuesToUpdate)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::ResendOtp::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	content := constants.OTP_CONFIRMATION_EMAIL_CONTENT
	content = strings.Replace(content, "NAME", userInfo.Fullname, -1)
	content = strings.Replace(content, "OTP", otpCode, -1)
	err = utils.SendMail(schema.Email, constants.OTP_CONFIRMATION_EMAIL_SUBJECT, content)

	if err != nil {
		response = &models.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
			Data:    "",
		}

		constants.LOGGER.Error(AUTH_CONTROLLER+"::ResendOtp::Error", err.Error())
		return c.JSONPretty(http.StatusBadRequest, response, " ")
	}

	response = &models.Response{
		Status:  http.StatusOK,
		Message: RESEND_OTP_CODE_RESEND_OTP_CODE_SUCCESSFULLY,
		Data:    "",
	}

	constants.LOGGER.Info(AUTH_CONTROLLER + "::ResendOtp::success")
	return c.JSONPretty(http.StatusOK, response, " ")
}
