package validations

type ConfirmOtpValidationSchema struct {
	OtpCode string `json:"otpCode" form:"otpCode" validate:"required"`
	Email   string `json:"email" form:"email" validate:"required,email"`
}
