package validations

type RefreshTokenValidationSchema struct {
	AccessToken  string `json:"accessToken" form:"accessToken" validate:"required"`
	RefreshToken string `json:"refreshToken" form:"refreshToken" validate:"required"`
}
