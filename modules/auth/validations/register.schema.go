package validations

type RegisterValidationSchema struct {
	Fullname          string `json:"fullname" form:"fullname" validate:"required"`
	Password          string `json:"password" form:"password" validate:"required"`
	ConfirmedPassword string `json:"confirmedPassword" form:"confirmedPassword" validate:"required"`
	Email             string `json:"email" form:"email" validate:"required,email"`
}
