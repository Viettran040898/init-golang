package validations

type ResendOtpValidationSchema struct {
	Email string `json:"email" form:"email" validate:"required,email"`
}
