package authentication

import (
	"github.com/labstack/echo/v4"
)

func AuthSubRoutes(group *echo.Group) {
	group.POST("/login", Login)
	group.POST("/register", Register)
	group.POST("/confirm", ConfirmOtp)
	group.POST("/refresh", RefreshToken)
	group.POST("/otp-code", ResendOtp)
	//group.POST("/otp-code", ResendOtp, middleware.CheckAccessToken(true))
}
