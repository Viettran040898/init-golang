package authentication

import (
	"fmt"
	"time"
	"todo_server/constants"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GenAccessToken(data primitive.ObjectID) (accessToken string, err error) {
	constants.LOGGER.Info(AUTH_SERVICE + "::GenAccessToken::is called")

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["userId"] = data
	//claims["admin"] = true
	claims["exp"] = time.Now().Add(time.Hour * 1).Unix()

	accessToken, err = token.SignedString([]byte(constants.SECRET_KEY))

	if err != nil {
		constants.LOGGER.Error(AUTH_SERVICE+"::GenAccessToken::error", err.Error())
		return "", err
	}

	constants.LOGGER.Info(AUTH_SERVICE + "::GenAccessToken::success")
	return accessToken, nil
}

func VerifyToken(tokenString string, isCheckTokenHasExpired bool) (userId primitive.ObjectID, err error) {
	constants.LOGGER.Info(AUTH_SERVICE + "::VerifyToken::is called")
	token, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			constants.LOGGER.Error(AUTH_SERVICE + "::VerifyToken::KeyFunc error")
			return nil, fmt.Errorf("KeyFunc error")
		}

		return []byte(constants.SECRET_KEY), nil
	})

	if err != nil {
		constants.LOGGER.Error(AUTH_SERVICE+"::VerifyToken::error", err.Error())

		if isCheckTokenHasExpired || err.Error() != "Token is expired" {
			return primitive.NilObjectID, err
		}
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	if !ok {
		constants.LOGGER.Error(AUTH_CONTROLLER + "::RefreshToken::Error")
		return primitive.NilObjectID, fmt.Errorf(REFRESH_TOKEN_INVALID_TOKEN)
	}

	data, ok := claims["userId"].(string)

	if !ok {
		constants.LOGGER.Error(AUTH_CONTROLLER + "::RefreshToken::Error")
		return primitive.NilObjectID, fmt.Errorf(REFRESH_TOKEN_INVALID_TOKEN)
	}

	userId, err = primitive.ObjectIDFromHex(data)

	if err != nil {
		constants.LOGGER.Error(AUTH_CONTROLLER+"::RefreshToken::Error", err.Error())
		return primitive.NilObjectID, err
	}

	constants.LOGGER.Info(AUTH_SERVICE + "::VerifyToken::success")
	return userId, nil
}
