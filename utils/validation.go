package utils

import (
	"todo_server/constants"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	constants.LOGGER.Info(constants.UTILS + "::Validate::is called")

	if err := cv.validator.Struct(i); err != nil {
		constants.LOGGER.Error(constants.UTILS+"::Validate::error", err.Error())
		return err
	}

	constants.LOGGER.Info(constants.UTILS + "::Validate::success")
	return nil
}

func ValidateData(c echo.Context, schema interface{}) error {
	constants.LOGGER.Info(constants.UTILS + "::ValidateData::is called")
	c.Echo().Validator = &CustomValidator{validator: validator.New()}

	if err := c.Bind(schema); err != nil {
		constants.LOGGER.Error(constants.UTILS+"::ValidateData::error", err.Error())
		return err
	}

	if err := c.Validate(schema); err != nil {
		constants.LOGGER.Error(constants.UTILS+"::ValidateData::error", err.Error())
		return err
	}

	constants.LOGGER.Info(constants.UTILS + "::ValidateData::success")
	return nil
}
