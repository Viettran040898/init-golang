package utils

import (
	"todo_server/constants"

	"gopkg.in/gomail.v2"
)

func SendMail(email string, subject string, content string) error {
	constants.LOGGER.Info(constants.UTILS + `::SendMail::is called`)
	mail := gomail.NewMessage()
	mail.SetHeader("From", "abcd040898.vt@gmail.com")
	mail.SetHeader("To", email)
	mail.SetHeader("Subject", subject)
	mail.SetBody("text/plain", content)
	dialer := gomail.NewDialer("smtp.gmail.com", 587, constants.EMAIL, constants.PASSWORD_MAIL)

	if err := dialer.DialAndSend(mail); err != nil {
		constants.LOGGER.Error(constants.UTILS+`::SendMail::error`, err.Error())
		return err
	}

	constants.LOGGER.Info(constants.UTILS + `::SendMail::success`)
	return nil
}
