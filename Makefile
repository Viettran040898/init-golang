build:
    go Build
	
install:
    go install

run: 
	nodemon --exec go run main.go --signal SIGTERM