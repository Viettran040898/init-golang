package main

import (
	"todo_server/constants"
	"todo_server/database"
	"todo_server/routes"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	e = *echo.New()
)

func main() {
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"https://labstack.com", "https://labstack.net"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	apiGroup := e.Group("/api")
	routes.ActivateIndex(apiGroup)

	client, err := database.DatabaseConnection()

	if err != nil {
		panic(err)
	}

	database.CollectionLinks(client)

	constants.LOGGER.Info(`Server listen at port `, constants.PORT, e.Start(constants.PORT))
}
