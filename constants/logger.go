package constants

import (
	"os"

	"github.com/withmandala/go-log"
)

var LOGGER = *log.New(os.Stderr)

const DATABASE = "DATABASE"
const DATABASE_SERVICE = "DATABASE_SERVICE"
const SERVICE = "SERVICE"
const UTILS = "UTILS"
const MIDDLEWARE = "MIDDLEWARE"
