package services

import (
	"todo_server/constants"

	"github.com/labstack/echo/v4"
)

func UseRoute(group *echo.Group, routes func(group *echo.Group)) {
	constants.LOGGER.Info(constants.SERVICE + "::UseRoute::is called")

	routes(group)

	constants.LOGGER.Info(constants.SERVICE + "::UseRoute::success")
}
