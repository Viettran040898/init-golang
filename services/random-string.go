package services

import (
	"math/rand"
	"todo_server/constants"
)

var letterRunes = []rune("01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) (randomStringResult string) {
	constants.LOGGER.Info(constants.SERVICE + `:RandStringRunes::is called`)
	b := make([]rune, n)

	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	constants.LOGGER.Info(constants.SERVICE + `:RandStringRunes::success`)
	return string(b)
}
