package database

import (
	"todo_server/constants"

	"go.mongodb.org/mongo-driver/mongo"
)

var (
	TodoListDatabase *mongo.Database
	UsersCollection  *mongo.Collection
)

func CollectionLinks(client *mongo.Client) {
	constants.LOGGER.Info(constants.DATABASE_SERVICE + "::CollectionLinks::is called")

	TodoListDatabase = client.Database("todoList")
	UsersCollection = TodoListDatabase.Collection("users")

	constants.LOGGER.Info(constants.DATABASE_SERVICE + "::CollectionLinks::success")
}
