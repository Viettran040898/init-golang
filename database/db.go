package database

import (
	"context"
	"time"
	"todo_server/constants"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)
)

func DatabaseConnection() (client *mongo.Client, err error) {
	constants.LOGGER.Info(constants.DATABASE + "::DatabaseConnection::is called")

	client, err = mongo.NewClient(options.Client().ApplyURI(constants.DB_URL))

	if err != nil {
		constants.LOGGER.Error(constants.DATABASE+"::DatabaseConnection::Error", err.Error())
		return nil, err
	}

	err = client.Connect(ctx)

	if err != nil {
		constants.LOGGER.Error(constants.DATABASE+"::DatabaseConnection::Could not connect to the database", err.Error())
		return nil, err
	}

	//defer client.Disconnect(constants.CTX)

	// err = client.Ping(ctx, readpref.Primary())

	// if err != nil {
	// 	logger.Error("database::ConnectionDatabase::Could not connect to the database", err.Error())
	// 	return err
	// }

	constants.LOGGER.Info(constants.DATABASE + "::DatabaseConnection::successful connection")
	return client, nil
}
