package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID             primitive.ObjectID `json:"_id, omitempty" bson:"_id"`
	Email          string             `json:"email" bson:"email"`
	Fullname       string             `json:"fullname" bson:"fullname"`
	PasswordHash   string             `json:"passwordHash" bson:"passwordHash"`
	RefreshToken   string             `json:"refreshToken" bson:"refreshToken"`
	OtpCode        string             `json:"otpCode" bson:"otpCode"`
	CreatedAt      time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt      time.Time          `json:"updatedAt" bson:"updatedAt"`
	Role           int                `json:"role" bson:"role"`
	IsConfirmedOtp bool               `json:"isConfirmedOtp" bson:"isConfirmedOtp"`
	IsBlocked      bool               `json:"isBlocked" bson:"isBlocked"`
}
